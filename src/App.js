import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import PostStory from "./Components/PostStory";
import OriginalStory from "./Components/OriginalStory";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={PostStory} />
          <Route path="/OriginalStory" component={OriginalStory} />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
