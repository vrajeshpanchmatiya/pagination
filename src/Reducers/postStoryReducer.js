import { storyType } from "../Actions/Type/storyType";

const initialState = {
  data: [],
};
// Reducer for store
export const postStoryReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case storyType:
      return {
        ...state,
        data: state.data.concat(payload),
      };
    default:
      return state;
  }
};
