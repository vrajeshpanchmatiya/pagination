import axios from "axios";

export const api_Story = (id) => {
  return axios.get(`${process.env.REACT_APP_API_URL}${id}`);
};
