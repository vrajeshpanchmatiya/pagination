import React from "react";
const OriginalStory = (props) => {
  const data = props.location.data;
  // JSON file of the Raw
  return <div>{JSON.stringify(data)}</div>;
};
export default OriginalStory;
