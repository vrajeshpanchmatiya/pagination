import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { storyAction } from "../Actions/storyAction";
import ReactTable from "react-table-v6";
import "react-table-v6/react-table.css";
import { Button } from "@material-ui/core";
import { Link } from "react-router-dom";
const PostStory = () => {
  const [id, setId] = useState(0);
  const dispatch = useDispatch();
  // useEffect for dispatch the id
  useEffect(() => {
    setTimeout(() => {
      dispatch(storyAction(id));
      setId(id + 1);
    }, 10000);
  });
  // useSelector for the fetching the data
  const detail = useSelector((state) => {
    return state.data;
  });
  //Table Columns name
  const columns = [
    {
      Header: "Title",
      accessor: "title",
      style: { backgroundColor: "aliceblue" },
    },
    {
      Header: "URL",
      accessor: "url",
      style: { backgroundColor: "lightblue" },
      sortable: false,
      filterable: false,
    },
    {
      Header: "CREATED_AT",
      accessor: "created_at",
      style: { backgroundColor: "yellow" },
    },
    {
      Header: "AUTHOR",
      accessor: "author",
      style: { backgroundColor: "salmon" },
    },
    {
      Header: "Action",
      style: { backgroundColor: "lightgrey" },
      sortable: false,
      filterable: false,
      Cell: (props) => {
        return (
          <Link
            to={{ pathname: "/OriginalStory", data: props.original }}
            style={{ textDecoration: "none" }}
          >
            <Button type="submit" variant="outlined" color="secondary">
              Fetch Row
            </Button>
          </Link>
        );
      },
    },
  ];
  // ReactTable for displaying the table
  return (
    <ReactTable
      columns={columns}
      noDataText="Please wait for a Seconds"
      defaultPageSize={20}
      showPageSizeOptions={false}
      filterable
      data={detail}
    />
  );
};
export default PostStory;
