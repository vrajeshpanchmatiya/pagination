import { storyType } from "./Type/storyType";
import { api_Story } from "../Services/api_Story";
// Action for calling api
export const storyAction = (id) => {
  return async (dispatch) => {
    const details = await api_Story(id);
    dispatch({ type: storyType, payload: details.data.hits });
  };
};
